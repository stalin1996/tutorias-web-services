<?php

namespace App\Http\Controllers;

use App\usuarioModel;
use Illuminate\Http\Request;
use Validator;
//para verificar si contraseña es válida
use Hash;
class usuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resultado=array('usuarios' => usuarioModel::all());
        return json_encode($resultado);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //primero establecer las valdaciones que se haran en cada campo enviado del request
        $validator = Validator::make($request->all(), [
        'idn'=> 'required|unique:usuario|max:20',
        'password'=>'required'
        ],
        //personalizar los mensajes a retornar par a cada error
        [
            'idn.required'=>'Es obligatorio enviar la idn',
            'idn.unique'=>'Este idn ya esta registrado'
        ]
        );
        //en caso de que detecte errores no intente guardar y retonre los mensajes de error
        if ($validator->fails())
        {    
        return response()->json($validator->messages(), 200);
        }

        
        try {
        //proceder a guardar en caso de que no haya errores
        $usuario_guardar=new usuarioModel();
        $usuario_guardar->idn=$request->idn;
        $usuario_guardar->apellidos=$request->apellidos;
        $usuario_guardar->nombres=$request->nombres;
        $usuario_guardar->rol_id=3;
        $usuario_guardar->password=bcrypt($request->password);
        $usuario_guardar->save();
        return 1;            
        } catch (\Illuminate\Database\QueryException $e) {
            return 10;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return usuarioModel::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function edit(usuarioModel $usuarioModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario_guardar=usuarioModel::find($id);
        $usuario_guardar->idn=$request->idn;
        $usuario_guardar->apellidos=$request->apellidos;
        $usuario_guardar->nombres=$request->nombres;
        $usuario_guardar->rol_id=$request->rol_id;
        $usuario_guardar->password=bcrypt($request->password);
        $usuario_guardar->save();
        return $usuario_guardar;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        usuarioModel::find($id)->delete();
        return 1;
    }
     public function login(Request $request){
        $idn= $request->idn;
        $password=$request->password;
        $usuario_logueado=usuarioModel::where('idn',$idn)->first();
        if(is_null($usuario_logueado)){
            //en caso de que no se encuentre un usuario con la idn enviada dsde la app
            return 404;
        }else{
            //si existe un usuario con la idn enviada entonces
             if (Hash::check($password, $usuario_logueado->password)){
                return  json_encode($usuario_logueado);
             //si la contraseña es correcta 
             }else{
                //caso contrario
                return 10;
             }



        }



     }
}
