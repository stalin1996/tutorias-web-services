<?php

namespace App\Http\Controllers;

use App\actividadModel;
use Illuminate\Http\Request;

class actividadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\actividadModel  $actividadModel
     * @return \Illuminate\Http\Response
     */
    public function show(actividadModel $actividadModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\actividadModel  $actividadModel
     * @return \Illuminate\Http\Response
     */
    public function edit(actividadModel $actividadModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\actividadModel  $actividadModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, actividadModel $actividadModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\actividadModel  $actividadModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(actividadModel $actividadModel)
    {
        //
    }
}
