<?php

namespace App\Http\Controllers;

use App\comunidadModel;
use Illuminate\Http\Request;

class comunidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\comunidadModel  $comunidadModel
     * @return \Illuminate\Http\Response
     */
    public function show(comunidadModel $comunidadModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\comunidadModel  $comunidadModel
     * @return \Illuminate\Http\Response
     */
    public function edit(comunidadModel $comunidadModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\comunidadModel  $comunidadModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, comunidadModel $comunidadModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\comunidadModel  $comunidadModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(comunidadModel $comunidadModel)
    {
        //
    }
}
