<?php

namespace App\Http\Controllers;

use App\rolModel;
use Illuminate\Http\Request;

class rolController extends Controller
{

    //metodo que se ejecuta por defecto al llamar url /roles
    public function index()
    {
        //obtengo todos los roles de la tabla rol de la base de datos configurada en el .env
        return rolModel::all(['id','descripcion']);
        //rolModel esta vinculada con la tabla rol
    }
       //metodo que se ejecuta por defecto al llamar url /roles/id
       public function show($id)
    {
        //obtengo un rol con la url ejm  roles/1 
        //return rolModel::find($id);
        //en caso de que solo quiera ciertos datos entonces se hace de la siguinte manera
        return rolModel::where('id',$id)        
        ->first(['id','descripcion']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    //metodo que se ejecuta por defecto al enviar un post a url /roles
    public function store(Request $request)
    {
        //creo una instancia de un registro de rol
        $rol_guardar= new rolModel();
        //le digo que el campo descripcion será igual a lo que yo envie desde el formulario o json con la propidad descripcion
        $rol_guardar->descripcion=$request->descripcion;
        //guardo en la tabla el registro
        $rol_guardar->save();
        //retorno el registro guardado, aqui ya sale el id cuando se guardó
        return $rol_guardar;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\rolModel  $rolModel
     * @return \Illuminate\Http\Response
     */
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\rolModel  $rolModel
     * @return \Illuminate\Http\Response
     */
    public function edit(rolModel $rolModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\rolModel  $rolModel
     * @return \Illuminate\Http\Response
     */
     //metodo que se ejecuta por defecto al enviar un put a url /roles
    public function update(Request $request, $id)
    {   
        //primero buscamos el objeto
        $rol_actualizar=rolModel::find($id);
        //actualizamos el campo o los campos del objeto encontrado
        $rol_actualizar->descripcion=$request->descripcion;
        //guardamos cambios en base de datos
        $rol_actualizar->save();
        //retornamos objeto actualizado
        return $rol_actualizar;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\rolModel  $rolModel
     * @return \Illuminate\Http\Response
     */

     //metodo que se ejecuta por defecto al enviar un delete a url /roles/id
    public function destroy($id)
    {
        //primero lo busco y luego lo elimino
        rolModel::find($id)->delete();
        return $id;
    }
}
