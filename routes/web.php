<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//ruta para el controlador de roles
Route::resource('roles','rolController');
//ruta para el controlador de usuarios
Route::resource('usuarios','usuarioController');
//ruta para el controlador de actividades
Route::resource('actividades','actividadController');
//ruta para el controlador de comunidades
Route::resource('comunidades','comunidadController');
//url para inicio de sesion (recibe usuario y contraseña)
Route::post('login','usuarioController@login');